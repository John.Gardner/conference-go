import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # what's needed to interact with the API (url, headers, params)
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": f"{city} {state}", "per_page": 1}

    # setting "response" to what we GET back from the request
    response = requests.get(url, params=params, headers=headers)

    # unpacking the JSON response.content to a python dict
    content = json.loads(response.content)

    # actual anti-corruption part.
    # Returning the "picture_url" when we call this function
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except KeyError:
        return {"picture_url": None}


def get_lat_lon(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }

    response = requests.get(url, params=params)
    content = response.json()

    lat = content[0]["lat"]
    lon = content[0]["lon"]

    return lat, lon


def get_weather_data(city, state):
    lat, lon = get_lat_lon(city, state)

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    response = requests.get(url, params=params)
    content = response.json()

    return {
        "description": content["weather"][0]["description"],
        "temp": content["main"]["temp"],
    }
